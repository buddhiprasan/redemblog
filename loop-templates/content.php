<?php
/**
 * Post rendering content according to caller of get_template_part
 *
 * @package redeblog
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
?>
<a class="card-link" href="<?php echo esc_url( get_permalink( ) ); ?>">
   <article class="single_card card single_card_blog">

		<header class="entry-header">
			<?php if ( has_post_thumbnail() ) : ?>
					<img class="img-fluid" src="<?php the_post_thumbnail_url(); ?>"/>
			<?php endif; ?>
		</header><!-- .entry-header -->

		<div class="entry-content entry-content_box">
			<span class="date_tab">
			<p><?php echo the_date(); ?></p>
			</span>
			<h2><?php echo the_title(); ?></h2>

			<?php
				$posttags = get_the_tags();
				$count=0; $sep='';
				if ($posttags) {
				echo '<ul class="tags-links list-inline">';
				foreach($posttags as $tag) {
					$count++;
					echo $sep. '<li class="list-inline-item">' .$tag->name. '</li>'; 
					if( $count > 1 ) break;
				}
				echo '</ul>';
				}
				?>


			<p><?php echo excerpt(18); ?></p>
		</div><!-- .entry-content -->

	<footer class="entry-footer">
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
</a>




