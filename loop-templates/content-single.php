<?php
/**
 * Single post partial template
 *
 * @package redeblog
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
?>

<article class="singel_post">

	<header class="entry-header">

	<?php if ( has_post_thumbnail() ) : ?>
	<img class="img-fluid" src="<?php the_post_thumbnail_url(); ?>"/>
	<?php endif; ?>
	
	<span>
	<?php the_date();?>
	</span>
   
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		
		<?php
                                    $posttags = get_the_tags();
                                    $count=0; $sep='';
                                    if ($posttags) {
                                    echo '<ul class="tags-links list-inline">';
                                    foreach($posttags as $tag) {
                                        $count++;
                                        echo $sep. '<li class="list-inline-item">' .$tag->name. '</li>'; 
                                        if( $count > 1 ) break;
                                    }
                                    echo '</ul>';
                                    }
                                    ?>
	</header><!-- .entry-header -->

	

	<div class="entry-content">

		<?php the_content(); ?>

	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<div class="blog_writer">
			<div class="blog_write_r">
			<?php 
				$image = get_field('write_image');
				if( !empty( $image ) ): ?>
					<img class="img-fluid" src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
				<?php endif; ?>
			</div>
			<div class="blog_write_l">
				<h4><?php the_field('blog_write_name'); ?></h4>
				<p><?php the_field('writer_position'); ?></p>
			</div>
		</div>
	</footer><!-- .entry-footer -->

</article><!-- #post-## -->
