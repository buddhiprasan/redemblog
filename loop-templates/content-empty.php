<?php
/**
 * Content empty partial template
 *
 * @package redeblog
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

the_content();
