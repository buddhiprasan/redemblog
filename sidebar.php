

	<div class="section_topic">
		<h2>Latest Post</h2>
	</div>



<?php
/**
 * The sidebar containing the main widget area
 *
 * @package redeblog
 */


$args = array(
    'posts_per_page'    => 4,
    'post_type'     => 'post',  //choose post type here
    'order' => 'DESC',
);

// The Query
$query = new WP_Query( $args );

// The Loop
if ( $query->have_posts() ) {
    while ( $query->have_posts() ) {
        $query->the_post();
?>
<!-- article -->
<a class="card-link" href="<?php echo esc_url( get_permalink( ) ); ?>">
   <article class="single_card card single_card_blog">

		<header class="entry-header">
			<?php if ( has_post_thumbnail() ) : ?>
					<img class="img-fluid" src="<?php the_post_thumbnail_url(); ?>"/>
			<?php endif; ?>
		</header><!-- .entry-header -->

		<div class="entry-content entry-content_box">
			<span class="date_tab">
			<p><?php echo the_date(); ?></p>
			</span>
			<h2><?php echo the_title(); ?></h2>

			<?php
				$posttags = get_the_tags();
				$count=0; $sep='';
				if ($posttags) {
				echo '<ul class="tags-links list-inline">';
				foreach($posttags as $tag) {
					$count++;
					echo $sep. '<li class="list-inline-item">' .$tag->name. '</li>'; 
					if( $count > 1 ) break;
				}
				echo '</ul>';
				}
				?>


			<p><?php echo excerpt(18); ?></p>
		</div><!-- .entry-content -->

	<footer class="entry-footer">
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
</a>
<?php
    }
} else {
    // no posts found
}

// Restore original Post Data
wp_reset_postdata();

?>