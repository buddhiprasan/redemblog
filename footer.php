<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package redeblog
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
?>

<footer class="site-footer" id="colophon">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-6 col-sm-12">
				<h2>2020 Redem GmbH. All Right Reserved</h2>
			</div>
			<div class="col-md-6 col-sm-12">
				<ul class="list-inline text-right">
					<li class="list-inline-item"><a href=""><i class="fab fa-facebook-f"></i></a>
						</li>
					<li class="list-inline-item"><a href=""><i class="fab fa-twitter"></i></a>
						</li>
				</ul>			
			</div>
		</div>
	</div>
</footer><!-- #colophon -->
<?php wp_footer(); ?>

</body>

</html>

