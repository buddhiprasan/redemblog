<?php
/**
 * The template for displaying all single posts
 *
 * @package redeblog
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-8 col-md-12">
	<!-- Do the left sidebar check -->
		<?php
		while ( have_posts() ) {
			the_post();
			get_template_part( 'loop-templates/content', 'single' );
		}
		?>
		</div>
		<div class="col-lg-4 col-md-12">
		<?php get_sidebar(); ?>
		</div>
	</div>
</div>

<?php
get_footer();
