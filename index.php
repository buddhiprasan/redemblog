<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package redeblog
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header('index');
?>

<?php get_template_part( 'loop-templates/content', 'featured' ); ?>
<?php get_template_part( 'loop-templates/content', 'trending' ); ?>

<section id="newsletter">
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-md-10">
				<h2>Subscribe Newsletter</h2>
				<p>Lorem Ipsum is simply dummy text of the printing and type the printing and typesetting industry setting industry</p>
				<div class="input-group">
					<input type="text" class="form-control" 
						>
					<div class="input-group-append">
						<button class="btn btn-outline-secondary" type="button" id="button-addon2">Subscribe</button>
					</div>
				</div>
			</div>
		</div>
	</div>

</section>

<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="section_topic">
				<h2>Latest Posts</h2>
			</div>
		</div>
	</div>

	<div class="row">
		<?php
				if ( have_posts() ) {
					// Start the Loop.
					while ( have_posts() ) {
						the_post();?>
		<div class="col-lg-4 col-md-6 col-sm-12">
			<?php get_template_part( 'loop-templates/content', get_post_format() ); ?>
		</div>
		<?php
					}
				} else {
					get_template_part( 'loop-templates/content', 'none' );
				}
				?>
	</div>
	<!-- The pagination component -->
	<?php redeblog_pagination(); ?>

	<!-- Do the right sidebar check -->
	<?php get_template_part( 'global-templates/right-sidebar-check' ); ?>

</div>
</div>

<?php
get_footer();