<?php
/**
 * redeblog functions and definitions
 *
 * @package redeblog
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

$redeblog_includes = array(
	'/theme-settings.php',                  // Initialize theme default settings.
	'/setup.php',                           // Theme setup and custom theme supports.
	'/widgets.php',                         // Register widget area.
	'/enqueue.php',                         // Enqueue scripts and styles.
	'/template-tags.php',                   // Custom template tags for this theme.
	'/pagination.php',                      // Custom pagination for this theme.
	'/hooks.php',                           // Custom hooks.
	'/extras.php',                          // Custom functions that act independently of the theme templates.
	'/customizer.php',                      // Customizer additions.
	'/custom-comments.php',                 // Custom Comments file.
	'/jetpack.php',                         // Load Jetpack compatibility file.
	'/class-wp-bootstrap-navwalker.php',    // Load custom WordPress nav walker. Trying to get deeper navigation? Check out: https://github.com/redeblog/redeblog/issues/567.
	'/woocommerce.php',                     // Load WooCommerce functions.
	'/editor.php',                          // Load Editor functions.
	'/deprecated.php',                      // Load deprecated functions.
);

foreach ( $redeblog_includes as $file ) {
	require_once get_template_directory() . '/inc' . $file;
}

if( function_exists('acf_add_local_field_group') ):

	acf_add_local_field_group(array(
		'key' => 'group_5f30334c7eb26',
		'title' => 'Blog Writer',
		'fields' => array(
			array(
				'key' => 'field_5f3033726bdce',
				'label' => 'Blog Write Name',
				'name' => 'blog_write_name',
				'type' => 'text',
				'instructions' => '',
				'required' => 1,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '100',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
			),
			array(
				'key' => 'field_5f3033df6bdcf',
				'label' => 'Writer Position',
				'name' => 'writer_position',
				'type' => 'text',
				'instructions' => '',
				'required' => 1,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
			),
			array(
				'key' => 'field_5f3036b26bdd0',
				'label' => 'Write Image',
				'name' => 'write_image',
				'type' => 'image',
				'instructions' => '',
				'required' => 1,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '100',
					'class' => '',
					'id' => '',
				),
				'return_format' => 'array',
				'preview_size' => 'large',
				'library' => 'all',
				'min_width' => '',
				'min_height' => '',
				'min_size' => '',
				'max_width' => '',
				'max_height' => '',
				'max_size' => 2,
				'mime_types' => '',
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'post',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => true,
		'description' => '',
	));
	
	endif;



	function excerpt($limit) {
		$excerpt = explode(' ', get_the_excerpt(), $limit);
  
		if (count($excerpt) >= $limit) {
			array_pop($excerpt);
			$excerpt = implode(" ", $excerpt) . '';
		} else {
			$excerpt = implode(" ", $excerpt);
		}
  
		$excerpt = preg_replace('`\[[^\]]*\]`', '', $excerpt);
  
		return $excerpt;
  }
  
  function content($limit) {
	  $content = explode(' ', get_the_content(), $limit);
  
	  if (count($content) >= $limit) {
		  array_pop($content);
		  $content = implode(" ", $content) . '';
	  } else {
		  $content = implode(" ", $content);
	  }
  
	  $content = preg_replace('/\[.+\]/','', $content);
	  $content = apply_filters('the_content', $content); 
	  $content = str_replace(']]>', ']]&gt;', $content);
  
	  return $content;
  }

	function count_post_visits() {
		if( is_single() ) {
		   global $post;
		   $views = get_post_meta( $post->ID, 'my_post_viewed', true );
		   if( $views == '' ) {
			  update_post_meta( $post->ID, 'my_post_viewed', '1' );   
		   } else {
			  $views_no = intval( $views );
			  update_post_meta( $post->ID, 'my_post_viewed', ++$views_no );
		   }
		}
	 }
	 add_action( 'wp_head', 'count_post_visits' );

